package principal;

import agente.RoboSensor;
import ambiente.Labirinto;
import utils.CoordenadaXY;

public class Principal {
    public static void main(String[] args) throws InterruptedException {
        
        Labirinto lab = new Labirinto(4);
        
        lab.exibirLabirinto();
        
        RoboSensor robo = new RoboSensor(lab);
        robo.setPosicaoXY(new CoordenadaXY(2,2)); //Definindo a posição inicial do robô
        
        
        while(robo.isAindaPercorrendo()){
            robo.zerarPilha();
            robo.movimentar();
            lab.exibirLabirinto();
            Thread.sleep(1500);
        }
    }
}
