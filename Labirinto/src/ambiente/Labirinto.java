package ambiente;

import agente.RoboSensor;
import java.util.Random;
import utils.CoordenadaXY;

public class Labirinto {
    
    private int tamanhoLabirinto;    
    private String[][] labirinto;
    private RoboSensor robo;
    
    public Labirinto(int tamanhoLabirinto) {
        this.tamanhoLabirinto = tamanhoLabirinto;     
        this.construirNovoLabirinto();
    }
     
    private void construirNovoLabirinto() {
        
       labirinto = new String[this.tamanhoLabirinto][this.tamanhoLabirinto];
       int linhaOcupada, colunaOcupada, posSaida;
       final String saida = "SAIDA";
       
        for (int i = 0; i < tamanhoLabirinto; i++) {
            for (int j = 0; j < tamanhoLabirinto; j++) {
                  labirinto[i][j] = StatusLabirinto.O.name();
            }
        }
        
            //Teste de criação de obstáculos (um L com 3 posições ocupadas)
//            labirinto[2][2] = StatusLabirinto.OCUPADO.name();
//            labirinto[2-1][2]= StatusLabirinto.OCUPADO.name();
//            labirinto[2][2+1]= StatusLabirinto.OCUPADO.name();
//            
//            labirinto[4][2] = StatusLabirinto.OCUPADO.name();
//            labirinto[4-1][2]= StatusLabirinto.OCUPADO.name();
//            labirinto[4-1][2-1]= StatusLabirinto.OCUPADO.name();
        
        //Definindo posições aleatórias para os obstáculos
//        Random r = new Random();
//        for (int k = 0; k < 16; k++) {
//            linhaOcupada = r.nextInt(tamanhoLabirinto);
//            colunaOcupada = r.nextInt(tamanhoLabirinto);
//            labirinto[linhaOcupada][colunaOcupada] = StatusLabirinto.O.name();
//        }
//        
//        //Definindo uma saída aleatória (em uma das paredes de baixo)
//        posSaida = r.nextInt(tamanhoLabirinto);
//        labirinto[this.tamanhoLabirinto-1][posSaida] = saida;
    }
    
    public void exibirLabirinto(){
        atualizarPosicaoRobo();
        for (int i = 0; i < tamanhoLabirinto; i++) {
               for (int j = 0; j < tamanhoLabirinto; j++) {
                   if(labirinto[i][j].equals(StatusLabirinto.R.name())){
                    System.out.print("|" + labirinto[i][j] + "|");
                } else {
                       System.out.print("| " + labirinto[i][j] + " |");
                }
            }
            System.out.println("");
        }
        System.out.println("");
    }
    public int getTamanhoLabirinto() {
        return tamanhoLabirinto;
    }

    public void setRobo(RoboSensor robo) {
        this.robo = robo;
    }
    
    public String retornarValorPosicaoLabirinto(CoordenadaXY posicao) {
        return this.labirinto[posicao.getPosX()][posicao.getPosY()];
    }
    
    public void definirNovaPosicao(){
        CoordenadaXY posicao = this.robo.getPosicaoXY();
        labirinto[posicao.getPosX()][posicao.getPosY()] = StatusLabirinto.L.name();
    }

    private void atualizarPosicaoRobo() {
        if(this.robo != null){
            CoordenadaXY posicaoAtual = this.robo.getPosicaoXY();
            labirinto[posicaoAtual.getPosX()][posicaoAtual.getPosY()] = StatusLabirinto.R.name();
        }
    }
}
