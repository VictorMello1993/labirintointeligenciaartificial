package agente;

//Essa classe será responsável pelo movimento no labirinto, em 4 direções 
//(cima, baixo, esquerda e direita)
import ambiente.Labirinto;
import ambiente.StatusLabirinto;
import utils.CoordenadaXY;

public class RoboSensor {

    private Labirinto labirinto;
    private Movimento movimento;
    private CoordenadaXY posicaoXY;
    private int pilhaMovimentos; /*Quantidade de movimentos realizados a cada chamada recursiva do método
                                   movimentar(), evitando erro de estouro de pilha*/

    public RoboSensor(Labirinto labirinto) {
       this.labirinto = labirinto;
       this.posicaoXY = new CoordenadaXY(); // Inicialmente o robô estará na posição (0,0), de cima para baixo  
       this.movimento = Movimento.CIMA; 
       labirinto.setRobo(this);       
    }
    
    //Modificar o método aqui
    /*Aqui será o coração do projeto, é responsável por toda a inteligência do robô para definir as próximas coordenadas (x,y),
    verificando se no meio do caminho existe algum obstáculo ou não antes de se movimentar*/
    public void movimentar(){
        
        /*Se o caminho estiver ocupado em todas as direções (cima, baixo, esquerda e direita),
        o robô não terá como se movimentar mais*/
        if(this.pilhaMovimentos >= 4){
            return;
        }
        
        CoordenadaXY proximoMovimento = retornarMovimento();
        String status = this.labirinto.retornarValorPosicaoLabirinto(proximoMovimento);
        
        if(status.equals(StatusLabirinto.L.name()) || status.equals(StatusLabirinto.R.name())){
            proximoMovimento();
            aumentarPilha();
            movimentar();
        } else {
           this.labirinto.definirNovaPosicao();
           this.posicaoXY = proximoMovimento;
        }  
    }
    
    private void aumentarPilha() {
        this.pilhaMovimentos++;
    }
    
    //Essa é a ordem de movimento do robô
     private void proximoMovimento() {
         switch(this.movimento){
             case CIMA:
                 this.movimento = Movimento.BAIXO;
                 break;
             case BAIXO:
                 this.movimento = Movimento.ESQUERDA;
                 break;
             case ESQUERDA:
                 this.movimento = Movimento.DIREITA;
                 break;
             case DIREITA:
                 this.movimento = Movimento.CIMA;
                 break;
         }
    }

    public CoordenadaXY retornarMovimento() {
        int retornoPosX = this.posicaoXY.getPosX();
        int retornoPosY = this.posicaoXY.getPosY();

        switch (movimento) {
            case CIMA:
                if (retornoPosX > 0) {
                    retornoPosX -= 1;
                }
                break;
            case BAIXO:
                if (retornoPosX < this.labirinto.getTamanhoLabirinto() - 1) {
                    retornoPosX += 1;
                }
                break;
            case ESQUERDA:
                if (retornoPosY > 0) {
                    retornoPosY -= 1;
                }
                break;
            case DIREITA:
                if (retornoPosY < this.labirinto.getTamanhoLabirinto() - 1) {
                    retornoPosY += 1;
                }
                break;
        }
        return new CoordenadaXY(retornoPosX, retornoPosY);
    }

    public CoordenadaXY getPosicaoXY() {
        return posicaoXY;
    }

    public void setPosicaoXY(CoordenadaXY posicaoXY) {
        this.posicaoXY = posicaoXY;
    }

    public boolean isAindaPercorrendo() {
        return pilhaMovimentos < 4;
    }

    public void zerarPilha() {
         this.pilhaMovimentos = 0;
    }
    
}
